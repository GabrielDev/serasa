# BUILD
FROM golang:1.13-stretch as builder

RUN mkdir /app
WORKDIR /app

COPY go.mod ./
RUN go mod download

COPY . .

# RUN go test -v ./...

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /serasa-api main.go

## PKG
FROM alpine

RUN apk update \
    && apk add --no-cache ca-certificates \
    && update-ca-certificates

ENV SERASA_DATABASE_ADDR ""
ENV SERASA_DATABASE_USER ""
ENV SERASA_DATABASE_PASSWORD ""
ENV SERASA_DATABASE_NAME "serasa"
ENV SERASA_LEGACY_API_HOST ""

COPY --from=builder /serasa-api /

CMD [ "/serasa-api", "serve" ]