# Serasa - API de Negativações

A API age como um intermediador para consumir uma aplicação legada (mainframe) de negativações que não está suportando a demanda atual.

## Instalação

### Pré-requisitos

Para executar, instale as seguintes dependência

- [Go](https://golang.org/doc/install)
- [MySQL](https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/)
- [Json Server (Recomendado)](https://www.npmjs.com/package/json-server)
- [Docker (Opcional)](https://docs.docker.com/get-docker/)

Crie uma base e um usuário no MySQL, os dados de conexão deverão ser repassados para aplicação

#### Localhost

Instale e construa o projeto

`go install; go build`

#### Docker

Construir

`docker build -t serasa .`

### Como executar

Antes de executar a aplicação, lembre-se de executar o simulador do servidor legado _(fake-api)_ com **Json Server** utilizando os dados mocados disponíveis [(negativacoes.json)](https://github.com/GabrielDev/serasa/blob/main/assets/server/negativacoes.json)

`json-server assets/server/negativacoes.json -p=3333 --host=<seu-ip> `

Verifique se está respondendo com o seguinte comando

```
curl --request GET \
  --url http://<seu-ip>:3333/negativations
```

Para mais informações de como manter os dados da _fake-api_, consulte a [documentação do Json Server](https://github.com/typicode/json-server).

#### Localhost

Variáveis para execução

| Variável          | Obrigatório | Padrão         | Descrição                                                     |
| ----------------- | ----------- | -------------- | ------------------------------------------------------------- |
| database-addr     | Sim         | localhost:3306 | Endereço da base de dados                                     |
| database-name     | Sim         | serasa         | Nome da base de dados                                         |
| database-password | Sim         |                | Senha da base de dados                                        |
| database-user     | Sim         | serasa         | Usuário da base de dados                                      |
| legacy-api-host   | Sim         |                | URL do servidor legado para coleta de dados                   |
| access-secret     | Não         | serasa         | Chave de geração de JWT                                       |
| port              | Não         | 8000           | Porta da API                                                  |
| log-level         | Não         | info           | Nível dos log (trace, debug, info, warn, error, fatal, panic) |

Executando

`go run . server --database-addr=<mysql-host>:<mysql-port> --database-name=<nome-base> --database-password=<senha> --database-user=<usuario> --legacy-api-host=<servidor-legado-host>:<servidor-legado-porta>`

#### Docker

Variáveis de ambiente

| Variável                 | Obrigatório | Padrão         | Descrição                                                     |
| ------------------------ | ----------- | -------------- | ------------------------------------------------------------- |
| SERASA_DATABASE_ADDR     | Sim         | localhost:3306 | Endereço da base de dados                                     |
| SERASA_DATABASE_NAME     | Sim         | serasa         | Nome da base de dados                                         |
| SERASA_DATABASE_USER     | Sim         |                | Senha da base de dados                                        |
| SERASA_DATABASE_PASSWORD | Sim         | serasa         | Usuário da base de dados                                      |
| SERASA_LEGACY_API_HOST   | Sim         |                | URL do servidor legado para coleta de dados                   |
| SERASA_ACCESS_SECRET     | Não         | serasa         | Chave de geração de JWT                                       |
| SERASA_PORT              | Não         | 8000           | Porta da API                                                  |
| SERASA_LOG_LEVEL         | Não         | info           | Nível dos log (trace, debug, info, warn, error, fatal, panic) |

Executando

```
docker run -p 8000:8000 \
-e SERASA_DATABASE_ADDR=<mysql-host>:<mysql-port> \
-e SERASA_DATABASE_USER=<nome-base> \
-e SERASA_DATABASE_PASSWORD=<senha> \
-e SERASA_DATABASE_NAME=<usuario> \
-e SERASA_LEGACY_API_HOST=<servidor-legado-host>:<servidor-legado-porta> \
-it serasa

```

Opcionalmente a instalação pode ser executada via docker-compose, mas lembre-se de atualizar essas variáveis no [docker-compose.yml](https://github.com/GabrielDev/serasa/blob/main/docker-compose.yml). Utilize o seguinte comando

`docker-compose up -d --build`

## Como utilizar

É possível consumir a API com qualquer cliente REST (Insomnia, Postman, etc), ou via curl. Um [arquivo](https://github.com/GabrielDev/serasa/blob/main/assets/insomnia.json) com todas as requisições está disponível, caso tenha o Insomnia instalado.

### Gerar token de autenticação

Todas as requisições deverão ser autenticadas. Para obter um token (com validade de 1h) utilize o seguinte comando

#### Requisição

```
curl --request GET \
  --url http://localhost:8000/token
```

#### Resposta

```
{
  "code": "OK",
  "message": "Successful Response",
  "error": "",
  "data": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2MDU2Mjg0MzMsInVzZXJfaWQiOjF9.LX8vyz_Iwmt9_31A6HcELJeb3AO7i7bKe4Gb07jDtCg"
}
```

### Sincronizar dados com servidor legado

Antes de realizar qualquer consulta, é necessário popular (sincronizar) a base deste serviço com os dados do servidor legado

#### Requisição

```
curl --request GET \
  --url http://localhost:8000/v1/refresh \
  --header 'Authorization: Bearer <token>'
```

#### Resposta

```
{
  "code": "OK",
  "message": "Successful Response",
  "error": "",
  "data": [
    {
      "contract": "bc063153-fb9e-4334-9a6c-0d069a42065b",
      "customerDocument": "51537476467",
      "companyDocument": "59291534000167",
      "companyName": "ABC S.A.",
      "value": 1235.23,
      "debtDate": "2015-11-13T20:32:51-03:00",
      "inclusionDate": "2020-11-13T20:32:51-03:00"
    },
    {
      "contract": "5f206825-3cfe-412f-8302-cc1b24a179b0",
      "customerDocument": "51537476467",
      "companyDocument": "77723018000146",
      "companyName": "123 S.A.",
      "value": 400,
      "debtDate": "2015-10-12T20:32:51-03:00",
      "inclusionDate": "2020-10-12T20:32:51-03:00"
    },
    {
      "contract": "3132f136-3889-4efb-bf92-e1efbb3fe15e",
      "customerDocument": "26658236674",
      "companyDocument": "04843574000182",
      "companyName": "DBZ S.A.",
      "value": 59.99,
      "debtDate": "2015-09-11T20:32:51-03:00",
      "inclusionDate": "2020-09-11T20:32:51-03:00"
    },
    {
      "contract": "8b441dbb-3bb4-4fc9-9b46-bdaad00a7a98",
      "customerDocument": "62824334010",
      "companyDocument": "23993551000107",
      "companyName": "XPTO S.A.",
      "value": 230.5,
      "debtDate": "2015-08-10T20:32:51-03:00",
      "inclusionDate": "2020-08-10T20:32:51-03:00"
    },
    {
      "contract": "d6628a0e-d4dd-4f14-8591-2ddc7f1bbeff",
      "customerDocument": "25124543043",
      "companyDocument": "70170935000100",
      "companyName": "ASD S.A.",
      "value": 10340.67,
      "debtDate": "2015-07-09T20:32:51-03:00",
      "inclusionDate": "2020-07-09T20:32:51-03:00"
    },
    {
      "contract": "bc8c54aa-2852-11eb-adc1-0242ac120002",
      "customerDocument": "25124543043",
      "companyDocument": "70170935000100",
      "companyName": "ASD S.A.",
      "value": 140.9,
      "debtDate": "2020-11-16T20:32:51-03:00",
      "inclusionDate": "2020-11-16T15:32:51-03:00"
    }
  ]
}
```

### Obter negativaçōes por CPF

Utilize um CPF válido para consultar as negativaçōes

#### Requisição

```
curl --request GET \
  --url 'http://localhost:8000/v1/negativations?cpf=<CPF>' \
  --header 'Authorization: Bearer <token>'
```

#### Resposta

```
{
  "code": "OK",
  "message": "Successful Response",
  "error": "",
  "data": [
    {
      "contract": "bc8c54aa-2852-11eb-adc1-0242ac120002",
      "customerDocument": "25124543043",
      "companyDocument": "70170935000100",
      "companyName": "ASD S.A.",
      "value": 140.9,
      "debtDate": "2020-11-16T23:32:51Z",
      "inclusionDate": "2020-11-16T23:32:51Z"
    },
    {
      "contract": "d6628a0e-d4dd-4f14-8591-2ddc7f1bbeff",
      "customerDocument": "25124543043",
      "companyDocument": "70170935000100",
      "companyName": "ASD S.A.",
      "value": 10340.67,
      "debtDate": "2015-07-09T23:32:51Z",
      "inclusionDate": "2020-07-09T23:32:51Z"
    }
  ]
}
```

### Ping

Opcionalmente foi disponibilizado um _endpoint_ para verificação de disponibilidade da aplicação

#### Requisição

```
curl --request GET \
  --url 'http://localhost:8000/ping'
```

#### Resposta

```
{
  "message": "pong"
}

```

## Testes

Antes de executar os testes, lembre-se de exportar todas as variáveis de ambiente e executar o simulador do servidor legado _(fake-api)_ com **Json Server**

```
export SERASA_DATABASE_ADDR=<mysql-host>:<mysql-port> \
export SERASA_DATABASE_USER=<usuario> \
export SERASA_DATABASE_PASSWORD=<senha> \
export SERASA_DATABASE_NAME=<nome-base> \
export SERASA_LEGACY_API_HOST=http://localhost:3334/negativations-test
```

#### Executando todos os testes

```
go test ./...
```
