package middlewares

import (
	"fmt"
	"net/http"
	"strings"

	"serasa.com.br/api/config"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// AuthMiddleware holds the default implementation of auth middleware
type AuthMiddleware struct {
	*config.Builder
}

// InitFromBuilder builds a AuthMiddleware instance
func InitFromBuilder(config *config.Builder) *AuthMiddleware {
	am := &AuthMiddleware{
		Builder: config,
	}
	return am
}

func (am AuthMiddleware) AuthRequired() gin.HandlerFunc {
	return func(c *gin.Context) {
		bearToken := c.Request.Header.Get("Authorization")
		if bearToken != "" {
			tokenString := strings.Split(bearToken, " ")[1]

			_, err := am.verifyToken(tokenString)
			if err != nil {
				logrus.Errorf("Error on introspect token: %s", err)
			} else {
				c.Next()
			}
		}
		c.AbortWithStatus(http.StatusUnauthorized)
	}
}

func (am AuthMiddleware) verifyToken(tokenString string) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(am.Builder.AccessSecret), nil
	})
	if err != nil {
		return nil, err
	}
	return token, nil
}
