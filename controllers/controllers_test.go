package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	"serasa.com.br/api/config"
	"serasa.com.br/api/models"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestCreateToken(t *testing.T) {
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)

	ctrl := InitFromBuilder(&config.Builder{
		Flags: &config.Flags{
			DatabaseName:     os.Getenv("SERASA_DATABASE_NAME"),
			DatabaseAddr:     os.Getenv("SERASA_DATABASE_ADDR"),
			DatabaseUser:     os.Getenv("SERASA_DATABASE_USER"),
			DatabasePassword: os.Getenv("SERASA_DATABASE_PASSWORD"),
			LegacyAPIHost:    os.Getenv("SERASA_LEGACY_API_HOST"),
			AccessSecret:     "TESTS",
		},
	})

	ctrl.CreateToken(c)
	assert.Equal(t, 200, w.Code, "Create token successfully responded")

	response := models.ResponseModel{}
	err := json.Unmarshal(w.Body.Bytes(), &response)
	if err != nil || (models.ResponseModel{}) == response {
		t.Errorf("Occurs an error on create token %s", err)
	}
}

func TestRecoverData(t *testing.T) {
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)

	ctrl := InitFromBuilder(&config.Builder{
		Flags: &config.Flags{
			DatabaseName:     os.Getenv("SERASA_DATABASE_NAME"),
			DatabaseAddr:     os.Getenv("SERASA_DATABASE_ADDR"),
			DatabaseUser:     os.Getenv("SERASA_DATABASE_USER"),
			DatabasePassword: os.Getenv("SERASA_DATABASE_PASSWORD"),
			LegacyAPIHost:    os.Getenv("SERASA_LEGACY_API_HOST"),
			AccessSecret:     "TESTS",
		},
	})

	ctrl.CreateToken(c)
	assert.Equal(t, 200, w.Code, "Create token successfully responded")

	response := models.ResponseModel{}
	err := json.Unmarshal(w.Body.Bytes(), &response)
	if err != nil || (models.ResponseModel{}) == response {
		t.Errorf("Occurs an error on create token %s", err)
	}

	token := response.Data
	negativation, err := insertNegativation(t)

	w = httptest.NewRecorder()
	c, _ = gin.CreateTestContext(w)
	c.Header("Content-type", "application/json")
	c.Header("Authorization", fmt.Sprintf("Bearer %s", token))

	ctrl.RecoverData(c)
	assert.Equal(t, 200, w.Code, "Recover data successfully responded")

	res := models.ResponseModel{}
	err = json.Unmarshal(w.Body.Bytes(), &res)
	if err != nil {
		t.Errorf("Occurs an error on recover data %s", err)
	}

	data := res.Data.([]interface{})
	lastInsered := data[len(data)-1].(map[string]interface{})
	assert.NotEqual(t, 0, len(data), "Recover data successfully listed")
	assert.Equal(t, negativation.Contract, lastInsered["contract"], "Recover data successfully insered")

	removeNegativation(t, ctrl, negativation.Contract)
}

func TestGetNegativations(t *testing.T) {
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)

	ctrl := InitFromBuilder(&config.Builder{
		Flags: &config.Flags{
			DatabaseName:     os.Getenv("SERASA_DATABASE_NAME"),
			DatabaseAddr:     os.Getenv("SERASA_DATABASE_ADDR"),
			DatabaseUser:     os.Getenv("SERASA_DATABASE_USER"),
			DatabasePassword: os.Getenv("SERASA_DATABASE_PASSWORD"),
			LegacyAPIHost:    os.Getenv("SERASA_LEGACY_API_HOST"),
			AccessSecret:     "TESTS",
		},
	})

	ctrl.CreateToken(c)
	assert.Equal(t, 200, w.Code, "Create token successfully responded")

	response := models.ResponseModel{}
	err := json.Unmarshal(w.Body.Bytes(), &response)
	if err != nil || (models.ResponseModel{}) == response {
		t.Errorf("Occurs an error on create token %s", err)
	}

	token := response.Data
	cpf := "99999999999"

	// Insert data
	insertNegativation(t)
	w = httptest.NewRecorder()
	c, _ = gin.CreateTestContext(w)
	c.Header("Content-type", "application/json")
	c.Header("Authorization", fmt.Sprintf("Bearer %s", token))
	ctrl.RecoverData(c)
	assert.Equal(t, 200, w.Code, "Recover data successfully responded")

	// List data
	w = httptest.NewRecorder()
	c, _ = gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest("GET", "?cpf="+cpf, nil)
	c.Header("Content-type", "application/json")
	c.Header("Authorization", fmt.Sprintf("Bearer %s", token))

	ctrl.GetNegativations(c)
	assert.Equal(t, 200, w.Code, "Recover data successfully responded")

	res := models.ResponseModel{}
	err = json.Unmarshal(w.Body.Bytes(), &res)
	if err != nil {
		t.Errorf("Occurs an error on recover data %s", err)
	}

	data := res.Data.([]interface{})
	negativation := data[0].(map[string]interface{})
	assert.NotEqual(t, 0, len(data), "Recover data successfully listed")
	assert.Equal(t, cpf, negativation["customerDocument"], "Recover data successfully matched")

	removeNegativation(t, ctrl, negativation["contract"].(string))
}

func insertNegativation(t *testing.T) (models.Negativation, error) {
	requestBody := strings.NewReader(`{
		"companyDocument": "99999999999999",
		"companyName": "Test S.A.",
		"customerDocument": "99999999999",
		"value": 1235.23,
		"contract": "bc063153-fb9e-4334-9a6c-0d069a42065b",
		"debtDate": "2020-11-20T20:00:00-03:00",
		"inclusionDate": "2020-11-20T20:00:00-03:00"
	}`)

	res, err := http.Post(
		os.Getenv("SERASA_LEGACY_API_HOST"),
		"application/json; charset=UTF-8",
		requestBody,
	)
	if err != nil {
		t.Errorf("Occurs an error on insert negativation on Legacy Server %s", err)
	}
	body, _ := ioutil.ReadAll(res.Body)

	negativation := models.Negativation{}
	err = json.Unmarshal(body, &negativation)
	if err != nil {
		t.Errorf("Occurs an error on unmarshall negativation %s", err)
	}
	defer res.Body.Close()

	return negativation, err
}

func removeNegativation(t *testing.T, ctrl *NegativationController, contract string) error {
	req, _ := http.NewRequest("DELETE", fmt.Sprintf("%s/%d", os.Getenv("SERASA_LEGACY_API_HOST"), 1), nil)
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Errorf("Occurs an error on delete negativation on Legacy Server %s", err)
	}
	response.Body.Close()

	err = ctrl.service.Provider.RemoveNegativationByContract(contract)
	if err != nil {
		t.Errorf("Occurs an error on remove negativation %s", err)
	}

	return err
}
