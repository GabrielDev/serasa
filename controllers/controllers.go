package controllers

import (
	"fmt"
	"net/http"
	"time"

	"serasa.com.br/api/config"
	"serasa.com.br/api/models"
	"serasa.com.br/api/services"
	"serasa.com.br/api/types"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/klassmann/cpfcnpj"
	"github.com/sirupsen/logrus"
)

// NegativationControllerAPI defines the available data apis
type NegativationControllerAPI interface {
	Ping(c *gin.Context)
	GetNegativations(c *gin.Context)
	RecoverData(c *gin.Context)
}

// NegativationController holds the default implementation of the Serasa API interface
type NegativationController struct {
	service *services.Service
	*config.Builder
}

// InitFromBuilder builds a NegativationController instance
func InitFromBuilder(config *config.Builder) *NegativationController {
	nc := &NegativationController{
		service: services.InitFromBuilder(config),
		Builder: config,
	}
	return nc
}

// Ping returns a pong
func (nc *NegativationController) Ping(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "pong",
	})
}

// Ping returns a pong
func (nc *NegativationController) CreateToken(c *gin.Context) {
	var err error
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = 1
	atClaims["exp"] = time.Now().Add(time.Minute * 60).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(nc.Builder.AccessSecret))
	if err != nil {
		logrus.Errorf("%s", err)
		c.JSON(http.StatusBadRequest, models.ResponseModel{
			Code:    "ERROR_GENERATE_TOKEN",
			Message: types.CodeMessage["ERROR_GENERATE_TOKEN"],
			Error:   err.Error(),
			Data:    "",
		})
		return
	}

	c.JSON(http.StatusOK, models.ResponseModel{
		Code:    "OK",
		Message: types.CodeMessage["OK"],
		Error:   "",
		Data:    token,
	})
}

// RecoverData handles get requests to sync all negativations from legacy server
func (nc *NegativationController) RecoverData(c *gin.Context) {
	negativations, err := nc.service.RecoverData()
	if err != nil {
		logrus.Errorf("%s", err)
		c.JSON(http.StatusBadRequest, models.ResponseModel{
			Code:    "ERROR_SYNC_NEGATIVATIONS",
			Message: types.CodeMessage["ERROR_SYNC_NEGATIVATIONS"],
			Error:   err.Error(),
			Data:    "",
		})
		return
	}

	c.JSON(http.StatusOK, models.ResponseModel{
		Code:    "OK",
		Message: types.CodeMessage["OK"],
		Error:   "",
		Data:    negativations,
	})
}

// GetNegativations handles get requests to get all negativations by CPF query parameter
func (nc *NegativationController) GetNegativations(c *gin.Context) {
	cpf, err := nc.checkQueryParams(c.Query("cpf"))
	if err != nil {
		logrus.Errorf("%s", err)
		c.JSON(http.StatusBadRequest, models.ResponseModel{
			Code:    "INVALID_CPF",
			Message: types.CodeMessage["INVALID_CPF"],
			Error:   err.Error(),
			Data:    "",
		})
		return
	}

	negativations, err := nc.service.GetNegativations(cpf)
	if err != nil {
		logrus.Errorf("%s", err)
		c.JSON(http.StatusBadRequest, models.ResponseModel{
			Code:    "ERROR_GET_NEGATIVATIONS",
			Message: types.CodeMessage["ERROR_GET_NEGATIVATIONS"],
			Error:   err.Error(),
			Data:    "",
		})
		return
	}

	logrus.Info(negativations)

	c.JSON(http.StatusOK, models.ResponseModel{
		Code:    "OK",
		Message: types.CodeMessage["OK"],
		Error:   "",
		Data:    negativations,
	})
}

func (nc *NegativationController) checkQueryParams(cpf string) (string, error) {
	err := cpfcnpj.ValidateCPF(cpf)
	if !err {
		return cpf, fmt.Errorf("The document is invalid")
	}

	return cpfcnpj.Clean(cpf), nil
}
