package routers

import (
	"log"
	"net/http"

	"serasa.com.br/api/config"
	"serasa.com.br/api/controllers"
	"serasa.com.br/api/middlewares"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// Server holds the information needed to run SerasaAPI
type Server struct {
	*config.Builder
	controller *controllers.NegativationController
	middleware *middlewares.AuthMiddleware
}

// InitFromWebBuilder builds a Server instance
func (s *Server) InitFromBuilder(config *config.Builder) *Server {
	s.Builder = config
	s.controller = controllers.InitFromBuilder(config)
	s.middleware = middlewares.InitFromBuilder(config)

	logLevel, err := logrus.ParseLevel(s.Builder.LogLevel)
	if err != nil {
		logrus.Errorf("Not able to parse log level string. Setting default level: info.")
		logLevel = logrus.InfoLevel
	}
	logrus.SetLevel(logLevel)

	return s
}

// Run initializes the web server and its apis
func (s *Server) Run() error {
	r := gin.Default()

	r.GET("/token", s.controller.CreateToken)
	r.GET("/ping", s.controller.Ping)

	v1 := r.Group("/v1", s.middleware.AuthRequired())
	{
		v1.GET("/refresh", s.controller.RecoverData)

		v1.GET("/negativations", s.controller.GetNegativations)
	}

	return r.Run("0.0.0.0:" + s.Builder.Port)
}

func notFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	log.Printf("Error status code: 404 when serving path: %s",
		r.RequestURI)
}
