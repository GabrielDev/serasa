package services

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"testing"

	"serasa.com.br/api/config"
	"serasa.com.br/api/models"

	"github.com/stretchr/testify/assert"
)

func TestRecoverData(t *testing.T) {
	service := InitFromBuilder(&config.Builder{
		Flags: &config.Flags{
			DatabaseName:     os.Getenv("SERASA_DATABASE_NAME"),
			DatabaseAddr:     os.Getenv("SERASA_DATABASE_ADDR"),
			DatabaseUser:     os.Getenv("SERASA_DATABASE_USER"),
			DatabasePassword: os.Getenv("SERASA_DATABASE_PASSWORD"),
			LegacyAPIHost:    os.Getenv("SERASA_LEGACY_API_HOST"),
		},
	})

	requestBody := strings.NewReader(`{
		"id": 1,
		"companyDocument": "99999999999999",
		"companyName": "Test S.A.",
		"customerDocument": "99999999999",
		"value": 1235.23,
		"contract": "bc063153-fb9e-4334-9a6c-0d069a42065b",
		"debtDate": "2020-11-20T20:00:00-03:00",
		"inclusionDate": "2020-11-20T20:00:00-03:00"
	}`)

	response, err := http.Post(
		os.Getenv("SERASA_LEGACY_API_HOST"),
		"application/json; charset=UTF-8",
		requestBody,
	)
	if err != nil {
		t.Errorf("Occurs an error on insert negativation on Legacy Server %s", err)
	}
	body, _ := ioutil.ReadAll(response.Body)

	negativation := models.Negativation{}
	err = json.Unmarshal(body, &negativation)
	if err != nil {
		t.Errorf("Occurs an error on unmarshall negativation %s", err)
	}
	defer response.Body.Close()

	res, err := service.RecoverData()
	if err != nil || len(res) == 0 {
		t.Errorf("Occurs an error on recovering negativations %s", err)
	} else {
		assert.Equal(t, negativation.Contract, res[0].Contract, "Negativation successfully saved")
	}

	req, _ := http.NewRequest("DELETE", fmt.Sprintf("%s/%d", os.Getenv("SERASA_LEGACY_API_HOST"), 1), nil)
	response, err = http.DefaultClient.Do(req)
	if err != nil {
		t.Errorf("Occurs an error on delete negativation on Legacy Server %s", err)
	}
	response.Body.Close()
}

func TestGetNegativations(t *testing.T) {
	service := InitFromBuilder(&config.Builder{
		Flags: &config.Flags{
			DatabaseName:     os.Getenv("SERASA_DATABASE_NAME"),
			DatabaseAddr:     os.Getenv("SERASA_DATABASE_ADDR"),
			DatabaseUser:     os.Getenv("SERASA_DATABASE_USER"),
			DatabasePassword: os.Getenv("SERASA_DATABASE_PASSWORD"),
			LegacyAPIHost:    os.Getenv("SERASA_LEGACY_API_HOST"),
		},
	})

	requestBody := strings.NewReader(`{
		"companyDocument": "99999999999999",
		"companyName": "Test S.A.",
		"customerDocument": "99999999999",
		"value": 1235.23,
		"contract": "bc063153-fb9e-4334-9a6c-0d069a42065b",
		"debtDate": "2020-11-20T20:00:00-03:00",
		"inclusionDate": "2020-11-20T20:00:00-03:00"
	}`)

	response, err := http.Post(
		os.Getenv("SERASA_LEGACY_API_HOST"),
		"application/json; charset=UTF-8",
		requestBody,
	)
	if err != nil {
		t.Errorf("Occurs an error on insert negativation on Legacy Server %s", err)
	}
	body, _ := ioutil.ReadAll(response.Body)

	negativation := models.Negativation{}
	err = json.Unmarshal(body, &negativation)
	if err != nil {
		t.Errorf("Occurs an error on unmarshall negativation %s", err)
	}
	defer response.Body.Close()

	res, err := service.GetNegativations(negativation.CustomerDocument)
	if err != nil || len(res) == 0 {
		t.Errorf("Occurs an error on get negativations %s", err)
	} else {
		result := res[0].(models.Negativation)
		assert.Equal(t, negativation.CustomerDocument, result.CustomerDocument, "Negativations successfully listed")
	}

	req, _ := http.NewRequest("DELETE", fmt.Sprintf("%s/%d", os.Getenv("SERASA_LEGACY_API_HOST"), 1), nil)
	response, err = http.DefaultClient.Do(req)
	if err != nil {
		t.Errorf("Occurs an error on delete negativation on Legacy Server %s", err)
	}
	response.Body.Close()
}
