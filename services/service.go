package services

import (
	"encoding/json"
	"fmt"
	"time"

	"serasa.com.br/api/config"
	"serasa.com.br/api/models"
	"serasa.com.br/api/providers"
	"serasa.com.br/api/web"

	"github.com/patrickmn/go-cache"
)

var (
	cacheNegativations = cache.New(15*time.Minute, 30*time.Minute)
)

// Service defines the methods that can be performed
type ServiceAPI interface {
	RecoverData() (negativations []models.Negativation, err error)
	GetNegativations(cpf string) ([]interface{}, error)
}

// Service has the methods and fields to handle the negativations persisting stuff
type Service struct {
	Provider *providers.Provider
	rest     web.REST
}

// InitFromBuilder builds a Service instance
func InitFromBuilder(config *config.Builder) *Service {
	provider, _ := providers.CreateProvider(
		config.Flags.DatabaseName,
		config.Flags.DatabaseAddr,
		config.Flags.DatabaseUser,
		config.Flags.DatabasePassword)

	return &Service{
		Provider: provider,
		rest:     web.InitFromBuilder(config),
	}
}

// RecoverData retrives negativations from legacy server
func (s *Service) RecoverData() (negativations []models.Negativation, err error) {
	body, err := s.rest.Get(nil)
	if err != nil {
		return nil, err
	}

	response := []models.Negativation{}
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, fmt.Errorf("Unmarshall error on get the response body, requesting Legacy Server API: %s", err)
	}

	for _, negativation := range response {
		_, err = s.Provider.CreateNegativationIfNotExists(negativation)
		if err != nil {
			return nil, fmt.Errorf("Negativation cannot be saved: %s", err)
		}
		negativations = append(negativations, negativation)
		cacheNegativations.Delete(negativation.CustomerDocument)
	}

	return negativations, nil
}

// GetNegativations get a list of negativations by CPF
func (s *Service) GetNegativations(cpf string) ([]interface{}, error) {
	cachedNegativations, found := cacheNegativations.Get(cpf)

	if !found {
		res, err := s.Provider.GetNegativationsByCPF(cpf)
		if err != nil {
			return nil, err
		}

		negativations := make([]interface{}, len(res))
		for i, v := range res {
			negativations[i] = v
		}

		cacheNegativations.Set(cpf, negativations, cache.DefaultExpiration)

		return negativations, err
	}

	return cachedNegativations.([]interface{}), nil
}
