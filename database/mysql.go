package database

import (
	"fmt"

	"serasa.com.br/api/types"

	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
)

var (
	databaseVersionCode = 0
	steps               = []string{
		"CREATE TABLE database_version (id INTEGER NOT NULL, current_version INTEGER NOT NULL, CONSTRAINT database_version_pk PRIMARY KEY (id));",
		"CREATE TABLE `" + types.NegativationCollectionName + "` ( `contract` varchar(36) NOT NULL, `customerDocument` varchar(11) NOT NULL, `companyDocument` varchar(14) NOT NULL, `companyName` varchar(200) NOT NULL, `value` double NOT NULL DEFAULT 0, `debtDate` DATETIME NOT NULL, `inclusionDate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`contract`), UNIQUE KEY `contract_UNIQUE` (`contract`)); ",
	}
)

//ExecuteMigration executes all creation queries.
func ExecuteMigration(conn *sqlx.DB) (int, error) {
	logrus.Infof("################ Database Migration Scripts ################")

	databaseVersionCode, _ := actualStep(conn)
	logrus.Infof("Actual Step: %v - Total Steps: %v", databaseVersionCode, len(steps))
	if databaseVersionCode >= len(steps) {
		logrus.Infof("Without database changes, skip migration\n")
		return 0, nil
	}
	logrus.Infof("Starting Migration Scripts")
	for step := databaseVersionCode; step < len(steps); step++ {
		res, err := conn.Exec(steps[step])
		if err != nil {
			logrus.Errorf("STEP %v : Unable to execute query `%v`: %v", step, steps[step], err)
			return 0, err
		}
		qtd, err := res.RowsAffected()
		if err != nil {
			logrus.Infof("STEP %v : Unable to execute RowsAffected `%v`: %v", step, steps[step], err)
			return 0, err
		}
		logrus.Infof("STEP %v : Executed query: `%v` : %v", step, steps[step], qtd)
		setActualStep(conn, step)
	}
	logrus.Infof("################ END OF Database Migration Scripts ################")
	return (len(steps) - databaseVersionCode), nil
}

// actualStep get actual version
func actualStep(conn *sqlx.DB) (int, error) {
	logrus.Trace("Get the actual version from database")
	var version int
	err := conn.QueryRow("SELECT current_version FROM database_version WHERE id=1;").Scan(&version)
	if err != nil {
		logrus.Trace("database_version table doesn't exists")
		return 0, err
	}
	version++
	logrus.Tracef("Actual Version: %v", version)
	return version, nil
}

// setActualStep set version
func setActualStep(conn *sqlx.DB, version int) error {
	logrus.Trace("Set the actual version to database")
	_, err := conn.Exec(
		`INSERT INTO database_version (id, current_version) VALUES (1, ?) ON DUPLICATE KEY UPDATE current_version = ?;`, version, version)
	if err != nil {
		logrus.Errorf("Error at insert the actual version of database schema")
		fmt.Println(err)
		return err
	}
	return nil
}
