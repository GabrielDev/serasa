package providers

import (
	"os"
	"testing"
	"time"

	"serasa.com.br/api/models"

	"github.com/stretchr/testify/assert"
)

func TestConnect(t *testing.T) {
	databaseName := os.Getenv("SERASA_DATABASE_NAME")
	databaseAddr := os.Getenv("SERASA_DATABASE_ADDR")
	databaseUser := os.Getenv("SERASA_DATABASE_USER")
	databasePassword := os.Getenv("SERASA_DATABASE_PASSWORD")
	_, err := CreateProvider(databaseName, databaseAddr, databaseUser, databasePassword)
	if err != nil {
		t.Errorf("Error connecting to mysql %s", err)
	}
}

func TestCreateNegativationIfNotExists(t *testing.T) {
	databaseName := os.Getenv("SERASA_DATABASE_NAME")
	databaseAddr := os.Getenv("SERASA_DATABASE_ADDR")
	databaseUser := os.Getenv("SERASA_DATABASE_USER")
	databasePassword := os.Getenv("SERASA_DATABASE_PASSWORD")
	provider, err := CreateProvider(databaseName, databaseAddr, databaseUser, databasePassword)
	if err != nil {
		t.Errorf("Error connecting to mysql %s", err)
	}

	date := time.Now()

	negativation := models.Negativation{
		Contract:         "bc063153-fb9e-4334-9a6c-0d069a42065b",
		CustomerDocument: "99999999999",
		CompanyDocument:  "99999999999999",
		CompanyName:      "Test SA",
		Value:            150.5,
		DebtDate:         date,
		InclusionDate:    date,
	}

	contract, err := provider.CreateNegativationIfNotExists(negativation)
	if err != nil {
		t.Errorf("Occurs an error on saving the negativation %s", err)
	}
	assert.Equal(t, negativation.Contract, contract, "Negativation successfully saved")

	negativation.Value = 1500

	_, err = provider.CreateNegativationIfNotExists(negativation)
	if err != nil {
		t.Errorf("Occurs an error on updating the negativation %s", err)
	}

	res, err := provider.GetNegativationsByCPF(negativation.CustomerDocument)
	if err != nil || len(res) == 0 {
		t.Errorf("Occurs an error on getting the negativation %s", err)
	}
	assert.Equal(t, negativation.Value, res[0].Value, "Negativation successfully updated")

	err = provider.RemoveNegativationByContract(negativation.Contract)
	if err != nil {
		t.Errorf("Occurs an error on removing the negativation %s", err)
	}
}

func TestGetNegativationsByCPF(t *testing.T) {
	databaseName := os.Getenv("SERASA_DATABASE_NAME")
	databaseAddr := os.Getenv("SERASA_DATABASE_ADDR")
	databaseUser := os.Getenv("SERASA_DATABASE_USER")
	databasePassword := os.Getenv("SERASA_DATABASE_PASSWORD")
	provider, err := CreateProvider(databaseName, databaseAddr, databaseUser, databasePassword)
	if err != nil {
		t.Errorf("Error connecting to mysql %s", err)
	}

	date := time.Now()

	negativation := models.Negativation{
		Contract:         "03476d1a-29b8-11eb-adc1-0242ac120002",
		CustomerDocument: "99999999999",
		CompanyDocument:  "99999999999999",
		CompanyName:      "Test SA",
		Value:            150.5,
		DebtDate:         date,
		InclusionDate:    date,
	}

	negativation2 := models.Negativation{
		Contract:         "0a477a05-2b7b-4858-9248-4c93470a1352",
		CustomerDocument: "99999999999",
		CompanyDocument:  "11111111111111",
		CompanyName:      "Quality SA",
		Value:            2000,
		DebtDate:         date,
		InclusionDate:    date,
	}

	_, err = provider.CreateNegativationIfNotExists(negativation)
	if err != nil {
		t.Errorf("Occurs an error on saving the negativation %s", err)
	}

	_, err = provider.CreateNegativationIfNotExists(negativation2)
	if err != nil {
		t.Errorf("Occurs an error on saving the negativation %s", err)
	}

	res, err := provider.GetNegativationsByCPF(negativation.CustomerDocument)
	if err != nil || len(res) == 0 {
		t.Errorf("Occurs an error on getting negativations by CPF %s", err)
	} else {
		assert.Equal(t, 2, len(res), "2 negativations saved")
		assert.Equal(t, negativation.Contract, res[0].Contract, "Contract successfully saved")
		assert.Equal(t, negativation.CustomerDocument, res[0].CustomerDocument, "CustomerDocument successfully saved")
		assert.Equal(t, negativation.CompanyDocument, res[0].CompanyDocument, "CompanyDocument successfully saved")
		assert.Equal(t, negativation.CompanyName, res[0].CompanyName, "CompanyName successfully saved")
		assert.Equal(t, negativation.Value, res[0].Value, "Value successfully saved")
		assert.Equal(t, negativation2.Contract, res[1].Contract, "Negativation 2 successfully saved")
	}

	err = provider.RemoveNegativationByContract(negativation.Contract)
	if err != nil {
		t.Errorf("Occurs an error on removing the negativation %s", err)
	}

	err = provider.RemoveNegativationByContract(negativation2.Contract)
	if err != nil {
		t.Errorf("Occurs an error on removing the negativation 2 %s", err)
	}
}
