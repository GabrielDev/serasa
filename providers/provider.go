package providers

import (
	"fmt"

	"serasa.com.br/api/database"
	"serasa.com.br/api/models"
	"serasa.com.br/api/types"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
)

// Provider holds the default implementation of DataProvider interface
type Provider struct {
	Client           *sqlx.DB
	alive            bool
	DatabaseAddr     string
	DatabaseName     string
	DatabaseUser     string
	DatabasePassword string
}

// CreateProvider initializes a default provider api instance
func CreateProvider(databaseName string, databaseAddr string, databaseUser string, databasePassword string) (*Provider, error) {
	p := &Provider{
		DatabaseName:     databaseName,
		DatabaseAddr:     databaseAddr,
		DatabaseUser:     databaseUser,
		DatabasePassword: databasePassword,
	}

	err := p.Connect()

	return p, err
}

// Connect to database
func (p *Provider) Connect() error {
	if !p.alive || p.Client == nil {
		logrus.Trace("Connect to database")
		logrus.Infof("Database URL: %v:*********@%v/%v", p.DatabaseUser, p.DatabaseAddr, p.DatabaseName)
		databaseURL := p.DatabaseUser + ":" + p.DatabasePassword + "@(" + p.DatabaseAddr + ")/" + p.DatabaseName + "?parseTime=true"
		db, err := sqlx.Connect("mysql", databaseURL)

		if err != nil {
			logrus.Errorf("Unable to connection to database: %s", err)
			panic(err)
		}

		p.Client = db
		p.alive = true
	}

	return nil
}

//Close all opened connections
func (p *Provider) Close() error {
	logrus.Infof("Closing DB connection")
	if p.Client != nil {
		p.alive = false
		return p.Client.Close()
	}
	return nil
}

// InitDatabase generate a Database from provider attributes
func (p *Provider) InitDatabase() error {
	err := p.Connect()
	if err != nil {
		logrus.Errorf("Error at creating MySQL client: %s", err)
		panic(err)
	}
	defer p.Close()
	_, err = database.ExecuteMigration(p.Client)
	if err != nil {
		logrus.Errorf("Error at executing MySQL Migration Script: %s", err)
		return err
	}
	return nil
}

// GetNegativationsByCPF get negativations by CPF
func (p *Provider) GetNegativationsByCPF(cpf string) (negativations []models.Negativation, err error) {
	qry := fmt.Sprintf("SELECT * FROM `%s` n WHERE n.customerDocument LIKE ?", types.NegativationCollectionName)
	err = p.Client.Select(&negativations, qry, cpf)
	if err != nil {
		return nil, fmt.Errorf("Occurs an error during get negativation: %s", err)
	}

	return negativations, nil
}

// GetNegativation get negativations by contract
func (p *Provider) GetNegativationByContract(idContract string) (string, error) {
	var contract []string
	qry := fmt.Sprintf("SELECT `contract` FROM `%s`n WHERE n.contract LIKE ?", types.NegativationCollectionName)
	err := p.Client.Select(&contract, qry, idContract)
	if err != nil {
		return "", fmt.Errorf("Occurs an error during get negativation: %s", err)
	}

	if len(contract) > 0 {
		return contract[0], nil
	}

	return "", nil
}

// CreateNegativationIfNotExists create negativation if not exists
func (p *Provider) CreateNegativationIfNotExists(negativation models.Negativation) (string, error) {
	contract, err := p.GetNegativationByContract(negativation.Contract)
	if err != nil {
		return "", err
	}

	if contract == "" {
		qry := fmt.Sprintf("INSERT INTO `%s` (contract, customerDocument, companyDocument, companyName, value, debtDate, inclusionDate) VALUES (:contract, :customerDocument, :companyDocument, :companyName, :value, :debtDate, :inclusionDate)", types.NegativationCollectionName)
		res, err := p.Client.NamedExec(qry, negativation)
		if err != nil {
			return "", fmt.Errorf("Error on insert negativation: %s", err)
		}

		logrus.Tracef("Response: %v", res)
		contract = negativation.Contract
	} else {
		qry := fmt.Sprintf("UPDATE `%s` SET customerDocument=:customerDocument, companyDocument=:companyDocument, companyName=:companyName, value=:value, debtDate=:debtDate, inclusionDate=:inclusionDate WHERE contract = :contract", types.NegativationCollectionName)
		res, err := p.Client.NamedExec(qry, negativation)
		if err != nil {
			return "", fmt.Errorf("Error on update negativation: %s", err)
		}

		logrus.Tracef("Response: %v", res)
	}

	return contract, nil
}

// RemoveNegativationByContract delete a negativation by contract
func (p *Provider) RemoveNegativationByContract(contract string) error {
	qry := fmt.Sprintf("DELETE FROM `%s`n WHERE n.contract LIKE ?", types.NegativationCollectionName)
	_, err := p.Client.Query(qry, contract)
	if err != nil {
		return fmt.Errorf("Occurs an error during remove negativation: %s", err)
	}
	return nil
}
