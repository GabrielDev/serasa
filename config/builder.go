package config

import (
	"fmt"

	"serasa.com.br/api/providers"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	port             = "port"
	logLevel         = "log-level"
	databaseName     = "database-name"
	databaseAddr     = "database-addr"
	databaseUser     = "database-user"
	databasePassword = "database-password"
	legacyAPIHost    = "legacy-api-host"
	accessSecret     = "access-secret"
)

// Flags define the fields that will be passed via cmd
type Flags struct {
	Port             string
	LogLevel         string
	DatabaseName     string
	DatabaseAddr     string
	DatabaseUser     string
	DatabasePassword string
	LegacyAPIHost    string
	AccessSecret     string
}

// Builder defines the parametric information of a Data API server instance
type Builder struct {
	*Flags
}

// AddFlags adds flags for Builder.
func AddFlags(flags *pflag.FlagSet) {
	flags.StringP(port, "p", "8000", "[optional] Custom port for accessing Data API's services. Defaults to 8000")
	flags.StringP(logLevel, "l", "info", "[optional] Sets the Log Level to one of seven (trace, debug, info, warn, error, fatal, panic). Defaults to info")
	flags.StringP(databaseName, "", "serasa", "[optional] Sets the name of Database where the data will be persisted. Defaults to serasa")
	flags.StringP(databaseAddr, "", "localhost:3306", "HTTP Address of Database. Defaults to localhost:3306")
	flags.StringP(databaseUser, "", "serasa", "Database connection authentication User. Defaults to serasa")
	flags.StringP(databasePassword, "", "", "Database connection authentication Password.")
	flags.StringP(legacyAPIHost, "", "", "Legacy API host used to recover negativation data.")
	flags.StringP(accessSecret, "", "serasa", "JWT access secret. Defaults to serasa.")
}

// Init initializes the web server builder with properties retrieved from Viper.
func (b *Builder) Init(v *viper.Viper) *Builder {
	flags := &Flags{
		Port:             v.GetString(port),
		LogLevel:         v.GetString(logLevel),
		DatabaseName:     v.GetString(databaseName),
		DatabaseAddr:     v.GetString(databaseAddr),
		DatabaseUser:     v.GetString(databaseUser),
		DatabasePassword: v.GetString(databasePassword),
		LegacyAPIHost:    v.GetString(legacyAPIHost),
		AccessSecret:     v.GetString(accessSecret),
	}

	flags.check()

	b.Flags = flags
	b.initDB()

	return b
}

// check required flags
func (flags *Flags) check() {
	requiredFlags := []struct {
		value string
		name  string
	}{
		{flags.DatabaseName, databaseName},
		{flags.DatabaseAddr, databaseAddr},
		{flags.DatabaseUser, databaseUser},
		{flags.DatabasePassword, databasePassword},
		{flags.LegacyAPIHost, legacyAPIHost},
	}

	var errMsg string
	for _, flag := range requiredFlags {
		if flag.value == "" {
			errMsg += fmt.Sprintf("\n\t%v", flag.name)
		}
	}

	if errMsg != "" {
		errMsg = "The following flags are missing: " + errMsg
		panic(errMsg)
	}
}

// initDB generate the database
func (b *Builder) initDB() {
	provider, err := providers.CreateProvider(
		b.Flags.DatabaseName,
		b.Flags.DatabaseAddr,
		b.Flags.DatabaseUser,
		b.Flags.DatabasePassword)

	if err == nil {
		provider.InitDatabase()
	}
}
