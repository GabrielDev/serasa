package models

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"time"
)

//Negativation Database object representation
type Negativation struct {
	Contract         string    `db:"contract" json:"contract"`
	CustomerDocument string    `db:"customerDocument" json:"customerDocument"`
	CompanyDocument  string    `db:"companyDocument" json:"companyDocument"`
	CompanyName      string    `db:"companyName" json:"companyName"`
	Value            float64   `db:"value" json:"value"`
	DebtDate         time.Time `db:"debtDate" json:"debtDate"`
	InclusionDate    time.Time `db:"inclusionDate" json:"inclusionDate"`
}

// ResponseModel holds the data that defines a response request from Negativation Controller
type ResponseModel struct {
	Code    string      `json:"code"`
	Message string      `json:"message"`
	Error   string      `json:"error"`
	Data    interface{} `json:"data"`
}

//JSONData defines a list of attributes.
type JSONData map[string]string

//Scan do the marshal over Database's query result.
func (jd *JSONData) Scan(val interface{}) error {
	switch v := val.(type) {
	case []byte:
		json.Unmarshal(v, &jd)
		return nil
	case string:
		json.Unmarshal([]byte(v), &jd)
		return nil
	default:
		return fmt.Errorf("Unsupported type: %T", v)
	}
}

//Value converts a Field to json.
func (jd *JSONData) Value() (driver.Value, error) {
	return json.Marshal(jd)
}
