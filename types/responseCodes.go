package types

var CodeMessage = map[string]string{
	"OK":                       "Successful Response",
	"DATA_CREATED":             "Negativation created",
	"INVALID_CPF":              "CPF invalid",
	"ERROR_GENERATE_TOKEN":     "Error to generate jwt token",
	"ERROR_GET_NEGATIVATIONS":  "Error to listing negativations",
	"ERROR_SYNC_NEGATIVATIONS": "Error to sync negativations with legacy server",
	"ERROR_SERVER_SAVE":        "Error to save negativation",
}
