package types

const (
	// NegativationCollectionName is the name of the collection that stores the negativations
	NegativationCollectionName string = "negativation"
)
