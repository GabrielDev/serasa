package web

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"serasa.com.br/api/config"

	"github.com/sirupsen/logrus"
)

// RESTInterface
type REST interface {
	Get(*http.Header) ([]byte, error)
}

// defaultREST holds the default implementation of REST interface
type defaultREST struct {
	uri    *url.URL
	client *http.Client
	header *http.Header
}

// InitFromBuilder initializes a default REST api instance
func InitFromBuilder(config *config.Builder) REST {
	instance := &defaultREST{}

	uri, err := url.Parse(config.LegacyAPIHost)
	if err != nil {
		logrus.Errorf("Invalid URL for REST-API.")
		panic(err)
	}
	instance.uri = uri

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	instance.client = &http.Client{Transport: tr}

	defautHeader := &http.Header{}
	defautHeader.Set("Content-Type", "application/json")
	defautHeader.Set("Accept", "*/*")
	instance.header = defautHeader

	return instance
}

// Get do http Get
func (d *defaultREST) Get(header *http.Header) ([]byte, error) {
	logrus.Traceln("Calling Legacy Server to get negotiation data on host %s", d.uri.String())
	req, _ := http.NewRequest("GET", fmt.Sprintf("%s", d.uri.String()), nil)

	if header == nil {
		req.Header = d.header.Clone()
	} else {
		req.Header = header.Clone()
	}
	logrus.Tracef("Payload %v", req)
	r, err := d.client.Do(req)

	if err != nil {
		return nil, fmt.Errorf("Couldn't REST Get, reason: %s", err)
	}
	defer r.Body.Close()
	if r.StatusCode >= 300 {
		return nil, fmt.Errorf("REST Get was not accepted, status code %d", r.StatusCode)
	}
	b, _ := ioutil.ReadAll(r.Body)
	return b, nil
}
