package cmd

import (
	"serasa.com.br/api/config"
	"serasa.com.br/api/routers"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Starts the Serasa API server",
	RunE: func(cmd *cobra.Command, args []string) error {
		builder := new(config.Builder).Init(viper.GetViper())
		server := new(routers.Server).InitFromBuilder(builder)

		err := server.Run()
		if err != nil {
			return err
		}

		return nil
	},
}

// init server with all command variables.
func init() {
	rootCmd.AddCommand(serveCmd)

	config.AddFlags(serveCmd.Flags())

	err := viper.GetViper().BindPFlags(serveCmd.Flags())
	if err != nil {
		panic(err)
	}
}
